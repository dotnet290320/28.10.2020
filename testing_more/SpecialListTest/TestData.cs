﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpecialListTest
{
    public static class TestData
    {

        public const int TestInitialize_Max_Special_List_Size = 100;

        // maximum size of special list
        public const int MySpecialList_Push_ExceedMax_MaxItems = 1000;

        public const int MySpecialList_Push_ExceedMax_OverflowItem = 1;

        public static int[] MySpecialList_Push_TestList = new int[]{2, 9, -3, 1, 5};
        public const int MySpecialList_Push_TestList_Max = 9;
        public const int MySpecialList_Push_TestList_Min = -3;
    }
}
