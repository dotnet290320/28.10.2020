﻿using System;
using System.Windows.Forms;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SpecialList;

namespace SpecialListTest
{
    [TestClass]
    public class MySpecialListTest
    {

        private MySpecialList special_list;

        [TestInitialize]
        public void TestInit()
        {
            // Runs EVERY TIME before EACH test

            //MessageBox.Show("TestInitialize");
            special_list = new MySpecialList(TestData.TestInitialize_Max_Special_List_Size);
        }

        [TestCleanup]
        public void TestCleanup()
        {
            // Runs every time AFTER each test

            //MessageBox.Show("TestCleanup"); 
            special_list.Clear();
        }

        [AssemblyInitialize]
        public static void Init(TestContext testContext)
        {
            // runs ONE TIME before 1st test
            //MessageBox.Show("AssemblyInitialize" + testContext.TestName);
        }

        [AssemblyCleanup]
        public static void Cleanup()
        {
            // runs ONE TIME after last test

            //MessageBox.Show("AssemblyCleanup");
        }

        /// <summary>
        /// Testing if MySpecial list will NOT accept
        /// a negative max length
        /// False positive test
        /// Only if ArgumentException will be thrown
        /// the test will pass
        /// </summary>
        [TestMethod]
        [Priority(0)]
        [ExpectedException(typeof(ArgumentException))]
        public void MySpecialList_Ctor_CanMaxBeNegative()
        {
            MySpecialList list = new MySpecialList(-10);
        }

        [TestMethod]
        [Priority(1)]
        public void MySpecialList_Count()
        {           
            //int max = 100;
            //MySpecialList list = new MySpecialList(max);

            // count when empty
            Assert.AreEqual(special_list.Count, 0);

            // count after adding item
            special_list.Push(10);
            Assert.AreEqual(special_list.Count, 1);

            // count after removing item --> not empty
            special_list.Push(20);
            Assert.AreEqual(special_list.Count, 2);
            special_list.PopFirst();
            Assert.AreEqual(special_list.Count, 1);

            // count after removing item --> empty
            special_list.PopFirst();
            Assert.AreEqual(special_list.Count, 0);

            // TODO : check if exceed the max, did the count
            //          stay as max ? or more ?
            //throw new NotImplementedException("Missing count test ...");
        }

        [TestMethod]
        public void MySpecialList_Push()
        {
            // push item
            // check if true was returned
            // check if same item is poped?
            int max = 100;
            MySpecialList list = new MySpecialList(max);
            bool push_12_result = list.Push(12);
            Assert.IsTrue(push_12_result);
            int pop1 = list.PopFirst();
            Assert.AreEqual(pop1, 12);

            // push items
            // push the same item again
            // check if false was returned
            list.Push(12);
            bool push_12_again_result = list.Push(12);
            Assert.IsFalse(push_12_again_result);
            // check count
            Assert.AreEqual(list.Count, 1);

            // push item: 5, 2, 9, -3, 1
            list.Clear();
            //list.Push(2);
            //list.Push(9);
            //list.Push(-3);
            //list.Push(1);
            foreach (int number in TestData.MySpecialList_Push_TestList)
            {
                list.Push(number);
            }

            // pop item from last - was 9 popped?
            // pop item from first - was -3 popped?
            int biggest = list.PopLast();
            int smallest = list.PopFirst();
            Assert.AreEqual(biggest, TestData.MySpecialList_Push_TestList_Max);
            Assert.AreEqual(smallest, TestData.MySpecialList_Push_TestList_Min);
        }

        [TestMethod]
        [ExpectedException(typeof(TooManyItemsException))]
        public void MySpecialList_Push_ExceedMax()
        {
            // if exceed max then TooManyItemsException ?
            int max = TestData.MySpecialList_Push_ExceedMax_MaxItems;
            MySpecialList list = new MySpecialList(max);

            for (int i = 0; i < TestData.MySpecialList_Push_ExceedMax_MaxItems; i++)
            {
                list.Push(i + 1);
            }

            list.Push(TestData.MySpecialList_Push_ExceedMax_OverflowItem);
        }

        [TestMethod]
        public void MySpecialList_PopFirst()
        {
            // pop from empty collection - did we get exception?
            // push 1 then 2 pops - did we get exception?
            // push 1 pop - same item?
        }
        [TestMethod]
        public void MySpecialList_PopLast()
        {
        }
        [TestMethod]
        public void MySpecialList_Clear()
        {

        }

    }
}
