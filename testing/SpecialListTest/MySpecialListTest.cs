﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SpecialList;

namespace SpecialListTest
{
    [TestClass]
    public class MySpecialListTest
    {
        /// <summary>
        /// Testing if MySpecial list will NOT accept
        /// a negative max length
        /// False positive test
        /// Only if ArgumentException will be thrown
        /// the test will pass
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void MySpecialList_Ctor_CanMaxBeNegative()
        {
            MySpecialList list = new MySpecialList(-10);
        }

        [TestMethod]
        public void MySpecialList_Count()
        {
            int max = 100;
            MySpecialList list = new MySpecialList(max);

            // count when empty
            Assert.AreEqual(list.Count, 0);

            // count after adding item
            list.Push(10);
            Assert.AreEqual(list.Count, 1);

            // count after removing item --> not empty
            list.Push(20);
            Assert.AreEqual(list.Count, 2);
            list.PopFirst();
            Assert.AreEqual(list.Count, 1);

            // count after removing item --> empty
            list.PopFirst();
            Assert.AreEqual(list.Count, 0);

            // TODO : check if exceed the max, did the count
            //          stay as max ? or more ?
            throw new NotImplementedException("Missing count test ...");
        }

        [TestMethod]
        public void MySpecialList_Push()
        {
            // push item
            // check if true was returned
            // check if same item is poped?

            // push items
            // push the same item again
            // check if false was returned 
            // check count

            // push item: 5, 2, 9, -3, 1
            // pop item from last - was 9 popped?
            // pop item from first - was -3 popped?
        }

        [TestMethod]
        public void MySpecialList_Push_ExceedMax()
        {
            // if exceed max then TooManyItemsException ?
        }

        [TestMethod]
        public void MySpecialList_PopFirst()
        {
            // pop from empty collection - did we get exception?
            // push 1 then 2 pops - did we get exception?
            // push 1 pop - same item?
        }
        [TestMethod]
        public void MySpecialList_PopLast()
        {
        }
        [TestMethod]
        public void MySpecialList_Clear()
        {
        }
    }
}
